# Variables

variable "scw_org" {}

variable "scw_token" {}

variable "prefix" {
  default = "yourname"
}

variable "rancher_version" {
  default = "latest"
}

variable "admin_username" {
  default = "admin"
}

variable "admin_password" {
  default = "admin"
}

variable "region" {
  default = "par1"
}

variable "docker_version_server" {
  default = "17.03"
}

variable "instance_server_type" {
  default = "C1"
}

variable "rancher_public_token" {}

variable "rancher_private_token" {}

variable "instance_agents_type" {}

# Provider
provider "scaleway" {
  organization = "${var.scw_org}"
  token        = "${var.scw_token}"
  region       = "${var.region}"
}

data "scaleway_image" "xenial" {
  architecture = "x86_64"
  name         = "Ubuntu Xenial"
}

resource "scaleway_ip" "rancher-server-ip" {
}

resource "scaleway_server" "rancherserver" {
  count               = "1"
  image               = "${data.scaleway_image.xenial.id}"
  type                = "${var.instance_server_type}"
  name                = "${var.prefix}-rancherserver"
  security_group      = "${scaleway_security_group.allowall.id}"
  public_ip           = "${scaleway_ip.rancher-server-ip.ip}"

  # Docker-compose
  provisioner "file" {
    source      = "${path.module}/files/stacks.tar.gz"
    destination = "/root/stacks.tar.gz"

    connection {
      type     = "ssh"
      user     = "root"
      private_key = "${file("/Users/lucasvagnier/.ssh/id_rsa")}"
    }
  }

  # Init script
  provisioner "file" {
    content      = "${data.template_file.init-server-script.rendered}"
    destination = "/root/init.sh"

    connection {
      type     = "ssh"
      user     = "root"
      private_key = "${file("/Users/lucasvagnier/.ssh/id_rsa")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /root/init.sh",
      "/root/init.sh"
    ]

    connection {
      type     = "ssh"
      user     = "root"
      private_key = "${file("/Users/lucasvagnier/.ssh/id_rsa")}"
    }
  }


}

//resource "scaleway_user_data" "rancher-server-init" {
//  server = "${scaleway_server.rancherserver.id}"
//  key    = "cloud-init"
//  value  = "${data.template_file.init-server-script.rendered}"
//}

# Install Rancher
data "template_file" "init-server-script" {
  template = "${file("${path.module}/files/userdata_server.sh")}"

  vars {
    admin_username        = "${var.admin_username}"
    admin_password        = "${var.admin_password}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
    rancher_server_url    = "${scaleway_ip.rancher-server-ip.ip}"
    rancher_public_token  = "${var.rancher_public_token}"
    rancher_private_token  = "${var.rancher_private_token}"
  }
}

resource "scaleway_security_group" "allowall" {
  name        = "rancher-server-allowall"
  description = "allow all traffic"
}

resource "scaleway_security_group_rule" "all_accept" {
  security_group = "${scaleway_security_group.allowall.id}"

  action    = "accept"
  direction = "inbound"
  ip_range  = "0.0.0.0/0"
  protocol  = "TCP"
}

output "rancher-url" {
  value = ["http://${scaleway_server.rancherserver.public_ip}"]
}

output "rancher-server-ip" {
  value = ["${scaleway_server.rancherserver.public_ip}"]
}
