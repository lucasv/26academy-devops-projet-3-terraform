# Agent #1
resource "scaleway_ip" "rancher-agent-1-ip" {
}

resource "scaleway_server" "rancher-agent-1" {
  count               = "1"
  image               = "${data.scaleway_image.xenial.id}"
  type                = "${var.instance_agents_type}"
  name                = "${var.prefix}-rancher-agent"
  security_group      = "${scaleway_security_group.allowall.id}"
  public_ip           = "${scaleway_ip.rancher-agent-1-ip.ip}"
}

resource "scaleway_user_data" "rancher-agent-1-cloud-init" {
  server = "${scaleway_server.rancher-agent-1.id}"
  key    = "cloud-init"
  value  = "${data.template_file.userdata_agent.rendered}"
}

# Agent #2
resource "scaleway_ip" "rancher-agent-2-ip" {
}

resource "scaleway_server" "rancher-agent-2" {
  count               = "1"
  image               = "${data.scaleway_image.xenial.id}"
  type                = "${var.instance_agents_type}"
  name                = "${var.prefix}-rancher-agent"
  security_group      = "${scaleway_security_group.allowall.id}"
  public_ip           = "${scaleway_ip.rancher-agent-2-ip.ip}"
}

resource "scaleway_user_data" "rancher-agent-2-cloud-init" {
  server = "${scaleway_server.rancher-agent-2.id}"
  key    = "cloud-init"
  value  = "${data.template_file.userdata_agent.rendered}"
}

# Install Rancher
data "template_file" "userdata_agent" {
  template = "${file("${path.module}/files/userdata_agent.sh")}"

  vars {
    admin_password        = "${var.admin_password}"
    docker_version_server = "${var.docker_version_server}"
    rancher_version       = "${var.rancher_version}"
    rancher_server_ip     = "${scaleway_ip.rancher-server-ip.ip}"
    rancher_public_token  = "${var.rancher_public_token}"
    rancher_private_token  = "${var.rancher_private_token}"
  }
}
