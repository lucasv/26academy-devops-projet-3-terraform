#!/bin/bash -x
export curlimage=appropriate/curl
export jqimage=stedolan/jq

export RANCHER_URL=http://127.0.0.1:80
export RANCHER_ACCESS_KEY=${rancher_public_token}
export RANCHER_SECRET_KEY=${rancher_private_token}

# Install Docker
if [ `command -v curl` ]; then
  curl -sL https://releases.rancher.com/install-docker/${docker_version_server}.sh | sh
elif [ `command -v wget` ]; then
  wget -qO- https://releases.rancher.com/install-docker/${docker_version_server}.sh | sh
fi

#for image in $curlimage $jqimage "rancher/server:${rancher_version}"; do
  #until docker inspect $image > /dev/null 2>&1; do
   # docker pull $image
  #  sleep 2
 # done
#done

docker run -d --restart=unless-stopped -p 80:8080 -p 443:443 -v /root/rancher:/var/lib/rancher rancher/server:${rancher_version}


ATTEMPTS=0
echo "Waiting for rancher..."
until curl --fail -s -X GET -H "Accept: application/json" "127.0.0.1/v2-beta/localauthconfig" ; do
  if ((ATTEMPTS >= 2000 )) ; then
  	exit
  fi
  ATTEMPTS=$[$ATTEMPTS+1]
  echo "Rancher not yet up, retrying..."
  sleep 1
done


# Create API Token
curl -X POST \
-H 'Accept: application/json' \
-H 'Content-Type: application/json' \
-d '{"type":"apikey", "accountId":"1a5", "publicValue":"${rancher_public_token}", "secretValue":"${rancher_private_token}"}' \
'http://127.0.0.1/v1/apikeys'

# Access control

curl -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d "\
{\
\"id\": null,\
\"type\": \"localAuthConfig\",\
\"baseType\": \"localAuthConfig\",\
\"accessMode\": \"unrestricted\",\
\"enabled\": true,\
\"name\": \"${admin_username}\",\
\"password\": \"${admin_password}\",\
\"username\": \"admin\"\
}" \
"127.0.0.1/v2-beta/localauthconfig"

# Rancher CLI
curl -L https://github.com/rancher/cli/releases/download/v0.6.13/rancher-linux-amd64-v0.6.13.tar.gz | tar -xvz
mv rancher-v0.6.13/rancher /usr/bin/.
rm -r rancher-v0.6.13

# Import stacks
tar -xvz -f /root/stacks.tar.gz
rancher stacks create Bracelet --docker-compose /root/stacks/bracelet/docker-compose.yml --rancher-compose /root/stacks/bracelet/rancher-compose.yml --start
rancher stacks create Datagod --docker-compose /root/stacks/datadog/docker-compose.yml --rancher-compose /root/stacks/datadog/rancher-compose.yml --start

