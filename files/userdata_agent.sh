#!/bin/bash -x
export curlimage=appropriate/curl
export jqimage=stedolan/jq

if [ `command -v curl` ]; then
  curl -sL https://releases.rancher.com/install-docker/${docker_version_server}.sh | sh
elif [ `command -v wget` ]; then
  wget -qO- https://releases.rancher.com/install-docker/${docker_version_server}.sh | sh
fi


ATTEMPTS=0
echo "Waiting for rancher..."
until curl --fail -s -X GET -H "Accept: application/json" "${rancher_server_ip}/v2-beta/localauthconfig" ; do
  if ((ATTEMPTS >= 2000 )) ; then
  	exit
  fi
  ATTEMPTS=$[$ATTEMPTS+1]
  echo "Rancher not yet up, retrying..."
  sleep 1
done

apt install -y jq

# Get project ID
PROJECT_ID=$(curl -s -u "${rancher_public_token}:${rancher_private_token}" http://${rancher_server_ip}/v1/projects | jq -r ".data[0].id")
sleep 1
# Create registration token
curl -s -X POST -u "${rancher_public_token}:${rancher_private_token}" http://${rancher_server_ip}/v1/registrationtokens?projectId=$PROJECT_ID
sleep 1
# Get registration token
TOKEN=`curl -s -u "${rancher_public_token}:${rancher_private_token}" http://${rancher_server_ip}/v1/registrationtokens?projectId=$PROJECT_ID | jq -r '.data[0].token'`

docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.11 http://${rancher_server_ip}/v1/scripts/$TOKEN

exit 0