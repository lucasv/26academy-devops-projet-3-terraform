# Scaleway provider configuration
scw_token = "xxxxxxxxxxxxxxxxx"
scw_org = "xxxxxxxxxxxxxxxxx"
region = "par1" # Region where resources should be created

# Resources will be prefixed with this to avoid clashing names
prefix = "26academy"

# rancher/rancher image tag to use
rancher_version = "latest"

# Docker version of host running `rancher/rancher`
docker_version_server = "17.03"

# Scaleway instance type
instance_server_type = "START1-S"
instance_agents_type = "START1-S"

# Rancher configuration
rancher_public_token = "public_token"
rancher_private_token = "secret_token_RDFGHJGFD"
admin_username= "admin"
admin_password = "admin"
